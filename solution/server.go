// Create webserver
// Add Prometheus metrics endpoint
// Calculate some dummy metrics

package main

import (
  "net/http"

  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
  opsProcessed = prometheus.NewCounter(prometheus.CounterOpts{
    Name: "myapp_ops_processed_total",
    Help: "The total number of operations processed",
  })
  opsErrorCount = prometheus.NewCounter(prometheus.CounterOpts{
    Name: "myapp_ops_errors_total",
    Help: "The total number of errors encountered",
  })
)

func init() {
  prometheus.MustRegister(opsProcessed)
  prometheus.MustRegister(opsErrorCount)
}

func main() {
  // Create webserver
  http.Handle("/metrics", promhttp.Handler())
  http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    opsProcessed.Inc()
    // Dummy operation
    _, err := dummyOp()
    if err != nil {
      opsErrorCount.Inc()
    }
    // Return result
  })
  http.ListenAndServe(":2112", nil)
}

func dummyOp() (string, error) {
  // Some dummy operation
  return "Dummy result", nil
}
