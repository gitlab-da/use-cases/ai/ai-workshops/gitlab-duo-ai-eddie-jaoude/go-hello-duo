// Create webserver
// Add Prometheus metrics endpoint
// Add a metrics count for HTTP requests on a specific endpoint 

package main

import (
  "net/http"

  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
  requestCounter = prometheus.NewCounterVec(
    prometheus.CounterOpts{
      Name: "http_requests_total",
      Help: "Count of HTTP requests",
    },
    []string{"path"},
  )
)

func init() {
  prometheus.MustRegister(requestCounter)
}

func main() {
  http.Handle("/metrics", promhttp.Handler())

  http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    requestCounter.With(prometheus.Labels{"path": r.URL.Path}).Inc()
    // Handle request
  })

  http.ListenAndServe(":8080", nil)
}
