// Create a function which queries a DNS resolver for a given hostname
// Call the function in main with gitlab.com
package main 

import (
  "fmt"
  "net"
)

func queryDNS(hostname string) {
  addr, err := net.LookupHost(hostname)
  if err != nil {
    fmt.Println("Error:", err)
    return
  }

  fmt.Printf("%s resolved to: %v\n", hostname, addr)
}

func main() {
  queryDNS("gitlab.com")
}
