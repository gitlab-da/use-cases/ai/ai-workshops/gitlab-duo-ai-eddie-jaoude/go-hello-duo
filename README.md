# Go Hello Duo

For instructions, follow https://gitlab.com/gitlab-da/use-cases/ai/ai-workshops/gitlab-duo-ai-eddie-jaoude/getting-started-exercises 

## Solution

cmd shift p to open a new terminal.

```shell
cd solution
```

### DNS

```shell
go build dns.go

./dns
```


### Server

Prometheus metrics

```shell
go build server.go

./server
```

2nd terminal 

```shell
curl localhost:2112

curl localhost:2112/metrics
```